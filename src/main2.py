import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject
import datetime

class MyWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Current Time Example")
        self.set_default_size(200, 100)

        self.label = Gtk.Label()
        self.add(self.label)

        # Schedule a function to update the label every second
        self.timeout_id = GObject.timeout_add_seconds(1, self.update_time)

    def update_time(self):
        # Update the label text with the current time
        now = datetime.datetime.now()
        self.label.set_text(now.strftime("%H:%M:%S"))
        # Return True to keep the timeout running
        return True

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
